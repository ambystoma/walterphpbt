<?php
// Database Config
$g_db_type = 'mysqli';
$g_hostname = '{MYSQL_DB_HOST}';
$g_database_name = '{MYSQL_DB_DATABASE}'
$g_db_username = '{MYSQL_DB_USER}';
$g_db_password = '{MYSQL_DB_PASSWORD}';
$g_db_table_prefix = 'phpbt_';
$g_default_timezone = 'America/Argentina/Buenos_Aires';

define ('TBL_ACTIVE_SESSIONS', TBL_PREFIX.'active_sessions');  // not exists???
define ('TBL_DB_SEQUENCE',     TBL_PREFIX.'db_sequence');      // not exists???
define ('TBL_ATTACHMENT',      TBL_PREFIX.'attachment');
define ('TBL_AUTH_GROUP',      TBL_PREFIX.'auth_group');
define ('TBL_AUTH_PERM',       TBL_PREFIX.'auth_perm');
define ('TBL_AUTH_USER',       TBL_PREFIX.'auth_user');
define ('TBL_BUG',             TBL_PREFIX.'bug');
define ('TBL_BUG_CC',          TBL_PREFIX.'bug_cc');
define ('TBL_BUG_DEPENDENCY',  TBL_PREFIX.'bug_dependency');
define ('TBL_BUG_GROUP',       TBL_PREFIX.'bug_group');
define ('TBL_BUG_HISTORY',     TBL_PREFIX.'bug_history');
define ('TBL_BUG_VOTE',        TBL_PREFIX.'bug_vote');
define ('TBL_COMMENT',         TBL_PREFIX.'comment');
define ('TBL_COMPONENT',       TBL_PREFIX.'component');
define ('TBL_CONFIGURATION',   TBL_PREFIX.'configuration');
define ('TBL_GROUP_PERM',      TBL_PREFIX.'group_perm');
define ('TBL_OS',              TBL_PREFIX.'os');
define ('TBL_PROJECT',         TBL_PREFIX.'project');
define ('TBL_RESOLUTION',      TBL_PREFIX.'resolution');
define ('TBL_SAVED_QUERY',     TBL_PREFIX.'saved_query');
define ('TBL_SEVERITY',        TBL_PREFIX.'severity');
define ('TBL_STATUS',          TBL_PREFIX.'status');
define ('TBL_USER_GROUP',      TBL_PREFIX.'user_group');
define ('TBL_USER_PERM',       TBL_PREFIX.'user_perm');
define ('TBL_USER_PREF',       TBL_PREFIX.'user_pref');
define ('TBL_VERSION',         TBL_PREFIX.'version');
define ('TBL_PROJECT_GROUP',   TBL_PREFIX.'project_group');
define ('TBL_PROJECT_PERM',    TBL_PREFIX.'project_perm');
define ('TBL_DATABASE',	       TBL_PREFIX.'database_server');
define ('TBL_SITE',            TBL_PREFIX.'site');
// New stuff for database DB_VERSION 5
define ('TBL_BOOKMARK',	       TBL_PREFIX.'bookmark');
define ('TBL_PRIORITY',        TBL_PREFIX.'priority');

// Constants
define('STRICT_ERROR_MODE', 1);
define ('ONEDAY', 86400);
define('PEAR_PATH', 'inc/pear/'); // Blank, or path to Pear::DB library directory containing DB.php (See http://pear.php.net/)
require_once (dirname(__FILE__).'/inc/auth.php');

// Mail parameters
define('SMTP_EMAIL', false);
define('SMTP_HOST', "localhost");
define('SMTP_PORT', 25);
define('SMTP_HELO', null);
define('SMTP_AUTH', false);
define('SMTP_AUTH_USER', "");
define('SMTP_AUTH_PASS', "");
define('RETURN_PATH', null);

