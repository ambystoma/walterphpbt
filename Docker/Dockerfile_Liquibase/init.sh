#!/bin/bash

#Se espera a que se inicie MySQL:
echo "Waiting for MySQL..."
while [ "$(mysqladmin ping --host=$MYSQL_DB_HOST --user=$MYSQL_DB_USER --password=$MYSQL_DB_PASSWORD --port=$MYSQL_DB_PORT)" != "mysqld is alive" ]; do
    sleep 3
done
echo "MySQL started"


mysql --host=$MYSQL_DB_HOST --user=$MYSQL_DB_USER --password=$MYSQL_DB_PASSWORD --port=$MYSQL_DB_PORT < /tmp/phpbugtracker.sql
